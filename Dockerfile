#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
# Latest runner version https://gitlab-runner-downloads.s3.amazonaws.com/latest/index.html
ENV VERSION=17.4.0

# Set base dependencies
ENV BASE_PKGS dumb-init git git-lfs openssl tzdata

# Update users and groups
RUN userdel ubuntu

# Update and install base dependencies
RUN apt-get update -q \
&& DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
&& DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
&& apt-get update -q \
&& DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
&& rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS curl

## Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS} \
 && rm -rf /var/lib/apt/lists/*

# Run build
WORKDIR /usr/src

RUN curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/v${VERSION}/binaries/gitlab-runner-linux-amd64
RUN chmod +x /usr/local/bin/gitlab-runner
RUN ln -s /usr/local/bin/gitlab-runner /usr/local/bin/gitlab-runner-helper


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN ln -s /usr/local/bin/gitlab-runner /usr/bin/gitlab-runner-helper

ENV RUNNER_EXECUTOR kubernetes
VOLUME ["/etc/gitlab-runner", "/var/lib/gitlab-runner"]
STOPSIGNAL SIGQUIT

ENTRYPOINT ["/usr/bin/dumb-init", "docker-entrypoint.sh"]
CMD ["gitlab-runner"]
