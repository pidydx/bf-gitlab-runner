#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    exec gitlab-runner register --non-interactive --template-config /etc/gitlab-runner/config.template.toml --config=/var/lib/gitlab-runner/.gitlab-runner/config.toml
fi

if [ "$1" = 'gitlab-runner' ]; then
    exec gitlab-runner run --user=gitlab-runner --working-directory=/var/lib/gitlab-runner --config=/var/lib/gitlab-runner/.gitlab-runner/config.toml
fi

exec "$@"